public class AccountAddressTriggerHelper {
    public static void addressPopulate(List<Account> listAccount){
        for(Account acc : listAccount){
            if(acc.Match_Billing_Address__c == True){
                acc.ShippingPostalCode = acc.BillingPostalCode;
            }
        }        
    }
}
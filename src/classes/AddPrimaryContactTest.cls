@isTest
private class AddPrimaryContactTest {
	@testSetup
    public static void datasetup(){
        List<Account> listAccount = new List<Account>();
        for(integer i= 1;i<=50;i++){
            Account acc = new Account();
            acc.name = 'TestNY'+ string.valueOf(i);
            acc.BillingState = 'NY';
            listAccount.add(acc);
        }
        for(integer i= 1;i<=50;i++){
            Account acc = new Account();
            acc.name = 'TestCA'+ string.valueOf(i);
            acc.BillingState = 'CA';
            listAccount.add(acc);
        }
        if(!listAccount.isEmpty()){
            insert listAccount;
        }
    }
    
    testMethod static void testqueueable(){
        test.startTest();
        Contact con = new contact(lastname='TestContactqueue');
        AddPrimaryContact abc = new AddPrimaryContact(con,'CA');
        ID queue = System.enqueueJob(abc);
        test.stopTest();
        System.assertEquals(50, [Select count() from contact where lastname = 'TestContactqueue']);
    }
}
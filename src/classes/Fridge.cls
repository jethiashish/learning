public class Fridge {
    public Fridge(){
        modelnumber = 'MX-0';
        numberinStock = 10;
    }
    public Integer ecoRating {
		get { return ecoRating; }
		set { ecoRating = value; if (ecoRating < 0) ecoRating =0; }
	}
    public string modelnumber;
    public integer numberinStock;
    
    public integer updateStock( integer itemsold) {
        numberinStock = numberinStock - itemsold;
        return numberinStock;
    }
}
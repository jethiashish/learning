@RestResource(urlMapping='/Account/*/Contact/')
global with sharing class AccountManager {
    @HttpGet
    global static Account getAccount(){
        RestRequest request = RestContext.request;
        //string regExp = 'Account/*/Contact';
        //list<String> listUrI = request.requestURI.split(regExp,3);
        string listURI1 = request.requestURI.substringbeforeLast('/');
        string listURI2 = listURI1.substring(listURI1.lastIndexOf('/') + 1);
        string AccountID = listUrI2;
        system.debug('ID passed in URL---' + listUrI1);
        system.debug('ID passed in URL---' + listUrI2);
        Account eachAccount = [select id, name ,(select id, name from Contacts) from Account where id =:AccountID];
        system.debug('AccountContact Details' + eachAccount);
        return eachAccount;
    }

}
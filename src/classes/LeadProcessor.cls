global class LeadProcessor implements Database.Batchable<Sobject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('Select id from Lead');
    }
    
    global void execute(Database.BatchableContext bc, List<Lead> scope){
        for(Lead eachLead : scope){
            eachLead.LeadSource = 'Dreamforce';
        }
        update scope;
    }    
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}
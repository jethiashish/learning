public with Sharing class myAccountExtensionPrac3 {
        public id accountNameText{get;set;}
    public boolean checkContact{get;set;}
    public boolean hideContactbutton{get;set;}
    public Account acc{get;set;}
    public list<Contact> resultContact{get;set;}
    public Account resultAccount{get;set;}
    
    public myAccountExtensionPrac3(ApexPages.StandardController controller) {
        this.acc = (Account)controller.getRecord();
        system.debug(this.acc);
        accountNameText = this.acc.Id;
        system.debug('this.acc.id'+accountNameText);
        checkContact = True;
        hideContactbutton =True;
    }
    public void getcontacts(){
        system.debug('this.acc.id'+accountNameText);
        resultAccount = [Select Id,(select firstname,lastname,Level__c,title from contacts) from Account where id =: accountNameText limit 1];
        system.debug('resultAccount'+resultAccount);
        resultContact = resultAccount.contacts;
        if(resultcontact.isEmpty()){
            checkContact = True;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Record to Display'));
            hideContactbutton =False;
        }
        else{
            checkContact = False;
            hideContactbutton =False;
            
        }
    }
    public PageReference getcontactsonPage(){
        getcontacts();
        return null;
    }
}
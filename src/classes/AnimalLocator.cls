public class AnimalLocator {
    public Static String getAnimalNameById(Integer num){
        String animalName;
        String endpoint = 'https://th-apex-http-callout.herokuapp.com/animals/'+num;//+num;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            system.debug('Results got: '+results);
            // Cast the values in the 'animals' key as a list
            Map<String, Object> animal = (Map<String, Object>) results.get('animal');
            system.debug('Animals got: '+animal);
            animalName =  (String)animal.get('name');
            system.debug('Animals got name: '+animalName);
        }
        return animalName;
    }
}
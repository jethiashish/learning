@isTest
private class TestRestrictContactByName {
    testmethod Static void checkContactName_positive(){
        
        Contact con1 = new Contact();
        con1.LastName = 'INVALIDNAME';
        Test.startTest();
        DataBase.SaveResult result = Database.insert(con1,False);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML', result.getErrors()[0].getMessage());
    }
    testmethod Static void checkContactName_negative(){
        
        Contact con1 = new Contact();
        con1.LastName = 'VALIDNAME';
        Test.startTest();
        DataBase.SaveResult result = Database.insert(con1,False);
        Test.stopTest();
        System.assertEquals(True, result.isSuccess());
    }
}
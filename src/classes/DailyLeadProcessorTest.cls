@isTest
private class DailyLeadProcessorTest {
	@testSetup
    public static void datasetup(){
        List<Lead> listLead = new List<Lead>();
        for(integer i= 1;i<=200;i++){
            Lead eachLead = new Lead();
            eachLead.LastName = 'TestLead'+i;
            eachLead.Company = 'Testcompany';
            listLead.add(eachLead);
        }
        if(!listLead.isEmpty()){
            insert listLead;
        }
    }
    testMethod static void testleadSchedule(){
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        test.startTest();
        DailyLeadProcessor abc = new DailyLeadProcessor();
        String jobId = System.schedule('ScheduledApexTest', CRON_EXP, new DailyLeadProcessor());
        test.stopTest();
        System.assertEquals(200, [Select count() from Lead where LeadSource = 'Dreamforce']);
    }
}
public class Opportunity_TriggerHelper {
    public static void updatingNSR(list<opportunity> listOpps){
        set<id> setofOpp = new set<id>();
        map<id,string> mapofStage = new map<id,String>();
        map<id,list<NSR__c>> mapofNSR = new map<id,list<NSR__c>>();
        list<NSR__c> allNSR = new List<NSR__c>();
        for(Opportunity eachOpp : listOpps){
            setofOpp.add(eachopp.id);
            mapofStage.put(eachopp.id, eachOpp.stagename);
        }
        
        list<opportunity> opportunityList = [Select ID,(select id,type__c from NSRwithOpportunity__r) from Opportunity where id IN: setofOpp];
        system.debug('Opportunity from SOQL-->'+opportunityList);
        for(Opportunity eachOpp: opportunityList){
            mapofNSR.put(eachOpp.id,eachOpp.NSRwithOpportunity__r);   
        }
        
        for(Opportunity eachOpp : listOpps){
            if(mapofStage.get(eachOpp.id) == 'Closed Lost'){
                list<NSR__c> listNSR = mapofNSR.get(eachOpp.id);
                for(NSR__c eachNSR : listNSR){
                    if(eachNSR.type__c == 'New' || eachNSR.type__c == 'Submitted'){
                        eachNSR.Type__c = 'Cancelled';
                        allNSR.add(eachNSR);
                    }
                }
            } 
        }
        
       	Database.SaveResult[] sresult = DataBase.update(allNSR,False);
        
        for(Database.SaveResult sr : sresult){
            if(sr.isSuccess()){
                System.debug('True');
            }
            else{
                System.debug('False');
            }
        } 
    }
    
    
    public static void checkingProduct(list<opportunity> listOpps){
        set<id> setofOpp = new set<id>();
        map<id,string> mapofStage = new map<id,String>();
        map<id,list<Opportunity_Product__c>> mapofProduct = new map<id,list<Opportunity_Product__c>>();
        map<id,id> mapofAggrement = new map<id,id>();
        list<Aggrement__c> allnewAggrement = new list<Aggrement__c>();
        list<Investment__c> allInvestment = new list<Investment__c>();
        for(Opportunity eachOpp : listOpps){
            setofOpp.add(eachopp.id);
            mapofStage.put(eachopp.id, eachOpp.stagename);
        }
        list<opportunity> opportunityList = [Select ID,(select id,type__c,Price__c,Quantity__c from Opportunity_Product__r) from Opportunity where id IN: setofOpp];
        for(Opportunity eachOpp : opportunityList){
            mapofProduct.put(eachOpp.id,eachOpp.Opportunity_Product__r);
        }
        system.debug('Map of Product-->'+mapofProduct);
        for(Opportunity eachOpp : listOpps){
            if(mapofStage.get(eachOpp.id) == 'Committed'){
                system.debug('mapofProduct-->'+mapofProduct.get(eachOpp.id));
                if(mapofProduct.get(eachOpp.id).isempty()){
                    eachOpp.addError('Opportunity has no Produts. Not Committed');
                }
                else{
					allnewAggrement.add(Opportunity_TriggerHelper.createAggrement(eachOpp));                   
                }
            }
        }
        if(!allnewAggrement.isEmpty()){
            Database.insert(allnewAggrement);
            
            list<Aggrement__c> newListAggrement =[Select id,Opportunity__c from Aggrement__c where Opportunity__c in:setofOpp];
            
            for(Aggrement__c eachAggrement : newListAggrement){
                mapofAggrement.put(eachAggrement.Opportunity__c,eachAggrement.id);
            }
            
            for(opportunity eachOpp: listOpps){
                list<investment__c> multipleInvest = new list<investment__c>();
                list<Opportunity_Product__c> products = mapofProduct.get(eachOpp.id);
                id oneAggrement = mapofAggrement.get(eachOpp.id);
                multipleInvest = Opportunity_TriggerHelper.createInvestment(products,oneAggrement);
                allInvestment.addAll(multipleInvest);
            }
            
            if(!allInvestment.isEmpty()){
            	Database.insert(allInvestment);
            }
        }
    }
    
    private static Aggrement__c createAggrement(Opportunity oneOpp){
        Aggrement__c newAggrement = new Aggrement__c();
        newAggrement.Account__c = oneOpp.Account.name;
        newAggrement.Name = oneOpp.Name+'-'+oneOpp.Account.name;
        newAggrement.Opportunity__c = oneOpp.id;
        newAggrement.Aggrement_Date__c = System.today();
        newaggrement.Amount__c = oneOpp.Amount;
        return newAggrement;
    }
    
    private static list<Investment__c> createInvestment(list<Opportunity_Product__c> listOpp,ID aggrement){
        list<Investment__c> allInvestment = new list<Investment__c>();
        for(Opportunity_Product__c eachOpp: listOpp){
            Investment__c oneInvestment = new Investment__c();
            oneInvestment.Aggrement__c = aggrement;
            oneInvestment.Opportunity_Product__c = eachOpp.Id;
            oneInvestment.Price__c = eachOpp.Price__c;
            oneInvestment.Quantity__c = eachOpp.Quantity__c;
            oneInvestment.Type__c = eachOpp.Type__c;
            allInvestment.add(oneInvestment);
        }
        return allInvestment;
    }
    
    
    /****NSR Code by Abhishek START*******/
    
    public static void updateRelatedNSR(list<Opportunity> opportunityList, map<Id,Opportunity> oldOppMap){
    	
    	set<Id> opportunityIdSet = new set<Id>();
    	
    	for(Opportunity opp : opportunityList){
    		if(opp.StageName == 'Closed Lost' && oldOppMap.get(opp.Id).StageName != 'Closed Lost'){
    			opportunityIdSet.add(opp.Id); // Create a set of Opportunity Ids to get NSR records
    		}
    	}
    	
    	// Get all the NSR records
    	list<NSR__c> nsrList = [SELECT Type__c FROM NSR__c WHERE Opportunity__c IN : opportunityIdSet AND Type__c IN ('New','Submitted')];
    	
    	if(!nsrList.isEmpty()){
    		for(NSR__c nsr : nsrList){
    			nsr.Type__c = 'Cancelled';
    		}
    		// Update NSR records
    		update nsrList;
    	}
    }
    
  /****NSR Code by Abhishek END*******/  
 
  
    
}
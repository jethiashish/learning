public class LeaseTriggerHelper{
	
    public static void updateExpirationDate(List<Lease__c> listRecords){
        list<AggregateResult> allLeaseList = new list<AggregateResult>();
        list<building__c> allBuildingList = new List<Building__c>();
        list<building__c> newBuildingList = new List<Building__c>();
        set<id> setofbuildingID = new set<ID>();
        map<ID,Date> mapofbuildingexpiration = new map<id,Date>();
        for(Lease__c eachRecord: Listrecords){
            setofbuildingID.add(eachRecord.Building__c);
        }
        
        allLeaseList = [select building__c,Min(Expiration_Date__c) expdate from Lease__c where Building__c IN:setofbuildingID group by Building__c];
        
        for(Aggregateresult eachRecord : allLeaseList){
            mapofbuildingexpiration.put((ID)eachRecord.get('building__c'),(Date)eachRecord.get('expdate'));
        }
        
        allBuildingList = [Select ID, expiration_date__c from building__C where id in:setofbuildingID];
        
        for(Building__c eachBuild : allBuildingList){
            eachBuild.expiration_date__c = mapofbuildingexpiration.get(eachBuild.Id);
            newBuildingList.add(eachBuild);
        }
        if(!newBuildingList.isEmpty()){
            Database.update(newBuildingList);
        }
    }
}
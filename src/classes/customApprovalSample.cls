public class customApprovalSample {
	/**
* This method will submit the opportunity automatically
**/
    public static void submitForApproval(Opportunity opp)
    {
        // Create an approval request for the Opportunity
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval automatically using Trigger');
        req1.setObjectId(opp.id);
        req1.setNextApproverIds(new Id[] {opp.Next_Approver__c});	 
        // Submit the approval request for the Opportunity
        Approval.ProcessResult result = Approval.process(req1);
    }
    /**
    * Get ProcessInstanceWorkItemId using SOQL
    **/
    public static Id getWorkItemId(Id targetObjectId)
    {
        Id retVal = null;
        for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p
            where p.ProcessInstance.TargetObjectId =: targetObjectId])
        {
            retVal  =  workItem.Id;
        }
        return retVal;
    }
    /**
    * This method will Approve the opportunity
    **/
    public static void approveRecord(Opportunity opp)
    {
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Approving request using Trigger');
        req.setAction('Approve');
        req.setNextApproverIds(new Id[] {opp.Next_Approver__c});
        Id workItemId = customApprovalSample.getWorkItemId(opp.id);
        if(workItemId == null)
        {
            opp.addError('Error Occured in Trigger');
        }
        else
        {
            req.setWorkitemId(workItemId);
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
        }
    }
    /**
    * This method will Reject the opportunity
    **/
    public static void rejectRecord(Opportunity opp)
    {
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setComments('Rejected request using Trigger');
        req.setAction('Reject');
        Id workItemId = getWorkItemId(opp.id);  
    
        if(workItemId == null)
        {
            opp.addError('Error Occured in Trigger');
        }
        else
        {
            req.setWorkitemId(workItemId);
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
        }
    }
}
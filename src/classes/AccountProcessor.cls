public class AccountProcessor {
    @future
    public static void countContacts(List<ID> ListAccountID){
        map<ID,List<ID>> mapofcontact = new map<ID,List<ID>>();
        List<Contact> listCon = [select id,accountid from contact where accountid IN: ListAccountID];
        for(Contact Con: listCon){
            if(mapofcontact.containsKey(Con.accountid)){
                list<ID> allCon = mapofcontact.get(Con.accountid);
                allCon.add(con.id);
                mapofcontact.put(Con.accountid, allCon);
            }
            else{
                list<ID> allCon = new List<ID>();
                allCon.add(con.id);
                mapofcontact.put(Con.accountid, allCon);
            }
        }
        system.debug('mapofContact-->'+mapofcontact);
        list<Account> listaccount = [Select id,number_of_contacts__c from account where id IN: ListAccountID];
        list<Account> listnewAcc = new list<Account>();
        for(Account acc: listaccount){
            if(mapofcontact.containsKey(acc.id)){
                list<ID> allCon = mapofcontact.get(acc.id);
                acc.number_of_contacts__c = allCon.size();
            }
            else{
                acc.number_of_contacts__c = 0;
            }
            listnewAcc.add(acc);
        }
        if(listnewAcc.size() > 0){
            Database.update(listnewAcc);
        }
    }
}
@isTest
public class ContactTriggerHelper_TestDataFactory {
    public Static Account createAccount(String name, boolean isActive){
        Account acc = new Account();
        acc.name = name;
        acc.isActive__c = isActive;
        Insert acc;
        return acc;
    }
    public Static Contact createContact(String Firstname, String Lastname,ID accountID){
        Contact con = new Contact();
        con.FirstName = Firstname;
        con.LastName = Lastname;
        con.AccountId = accountID;
        Insert con;
        return con;
    }
    public Static void createBulkContact(ID accountID){
        List<Contact> listContact = new List<Contact>();
        for(integer i = 1;i<=5;i++){
            Contact con = new Contact();
            con.LastName = 'NewTestDataContact'+ String.valueof(i);
            con.AccountId = accountID;
            system.debug('List contact-->'+con);
            listContact.add(con);
        }
        
        Insert listContact;
    }
}
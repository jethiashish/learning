public class myAccountPOC {
    public ID acc{get;set;}
    public list<Contact> contactList;
    public list<opportunity> opportunityList;
    public list<case> CaseList;
    public Apexpages.StandardSetController setCon{get;set;}
    public Apexpages.StandardSetController setCon2{get;set;}
    public Apexpages.StandardSetController setCon3{get;set;}
    
        public myAccountPOC(ApexPages.StandardController controller) {
        this.acc = (ID)controller.getRecord().id;
        getContacts(acc);
        getOpportunity(acc);
        getCases(acc);
    }
    
    Private void getContacts(ID accountID){
        contactList = [Select ID,name,account.name,phone,leadSource from contact where accountid =: accountID];
        setcon = new apexpages.StandardSetController(contactlist);
        setcon.setpageSize(2);
    }
    
    Private void getOpportunity(ID accountID){
        opportunityList = [Select ID,amount,name,account.name,stageName,leadSource,TotalOpportunityQuantity,Probability from opportunity where accountid =: accountID];
    	setcon2 = new apexpages.StandardSetController(Opportunitylist);
        setcon2.setpageSize(2);
    }
    
    Private void getCases(ID accountID){
        caseList = [Select ID,casenumber,status,type,Priority from case where accountid =: accountID];
        setcon3 = new apexpages.StandardSetController(Caselist);
        setcon3.setpageSize(2);
    }
    
    Public List<Contact> getcontactList(){
        return (list<Contact>) setCon.getRecords();
    }
    
    Public List<Opportunity> getOpportunityList(){
        return (list<Opportunity>) setCon2.getRecords();
    }

    Public List<Case> getCaseList(){
        return (list<Case>) setCon3.getRecords();
    }
    
}
public with sharing class BuildingController {
    
    public Building__c BuildingRecord{get;set;}
    public Boolean newBuildingPage{get;set;}
    public Apexpages.StandardSetController setcon{get;set;}
    list<Buildingwrapper> buildingwrapperList{get;set;}
    Public List<Building__c> buildingList;
    public List<lease__c> leaseList{get;set;}
    public boolean showlease{get;set;}
    public boolean showinfo{get;set;}
    public boolean showcheckError{get;set;}
    public id leaseidselected{get;set;}
    public Lease__c LeaseRecord{get;set;}
    public Boolean selectedrecord{get;set;}
    public Boolean SelectedLease{get;set;}
    public Boolean LeaseEditButton{get;set;}
    
    public BuildingController(){
        SelectedLease = False;
        LeaseEditButton = False;
        newBuildingPage = False;
        showlease = False;
        showinfo = False;
        showcheckError = False;
        BuildingRecord = new Building__c();
        getBuilding();
    }
    
    public Pagereference Save(){
        Database.upsert(BuildingRecord);
        BuildingRecord = new Building__c();
        return null;
    }
    
    public Pagereference Cancel(){
        BuildingRecord = new Building__c();
        newBuildingPage = False;
        return null;
    }
    
    public Pagereference getnewBuildingPage(){
        newBuildingPage = True;
        return null;
    }
    
    private void getBuilding(){
        buildingList = [select id,name,expiration_date__c,status__c from Building__c];
        setcon = new Apexpages.StandardSetController(buildingList);
        setcon.setpagesize(2);
    }
    
    public list<buildingwrapper> getbuildingData(){
        buildingwrapperList = new list<Buildingwrapper>();
        list<building__c> newList = (list<Building__c>) setcon.getRecords();
        system.debug('New List'+newList);
        if(!newList.isEmpty()){
            for(Building__c eachbuilding : newList){
                buildingwrapperList.add(new buildingwrapper(eachBuilding,False));
            }
        }
        return buildingwrapperList; 
    }
    
    public Pagereference showleasedata(){
        Integer count = 0;
        system.debug('buildingwrapperList'+buildingwrapperList);
        for(buildingwrapper bw : buildingwrapperList){
            if(bw.selected == True){
                count = count + 1;
                leaseList =[select id, name,building__c,expiration_date__c,status__c from lease__c where building__c =:bw.Buildingrecord.Id];   
            }
        }
        if(count > 1){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Select only one Record'));
            showcheckError = True;
            showinfo = False;
            showlease = False;
        }
        else{
            if(leaseList.isEmpty()){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No Records to Display'));
                showinfo = True;
                showlease = False;
                showcheckError = False;
                SelectedLease = False;
            }
            else{
                showlease = True;
                showinfo = False;
                showcheckError = False;
            }
        }
        return null;
    }
    
    public Pagereference SaveInlineBuilding(){
        List<Building__c> buildingList = new List<Building__c>();
        system.debug('buildingwrapperList'+buildingwrapperList);
        for(buildingwrapper bw : buildingwrapperList){
            buildingList.add(bw.Buildingrecord);
        }
        if(!buildingList.isEmpty()){
            Database.update(buildingList);
            getBuilding();
        }
        return ApexPages.CurrentPage();
    }
    
    public void getleaserecord(){
                LeaseRecord = [select id,name,expiration_date__c,status__c from lease__c where id =: leaseidselected];
        SelectedLease = True;
    }
    public Pagereference updateLeaseRecord(){
        database.update(LeaseRecord);
        showleasedata();
        SelectedLease = False;
        return null;
    }
    public Pagereference EditLeaseRecord(){
        LeaseEditButton = True;
        return null;
    }
    public Pagereference CancelLeaseRecord(){
        LeaseEditButton = False;
        getleaserecord();
        return null;
    }
    
    public class buildingwrapper{
        public boolean selected{get;set;}
        public Building__c Buildingrecord{get;set;}
        
        public buildingwrapper(Building__c eachbuilding,Boolean isselected){
            this.Buildingrecord = eachbuilding;
            this.selected = isselected;
        }
    }
}
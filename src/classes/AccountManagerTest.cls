@IsTest
private class AccountManagerTest {

    @isTest static void testgetAccountById() {
        Id recordId = createTestRecord();
        contact contactRecord = createTestContact(recordId);
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://ap4.salesforce.com/services/apexrest/Account/'+ recordId + '/Contact';
        
        system.debug('request.requestUri' + request.requestUri);
        request.httpMethod = 'GET';
        RestContext.request = request;
        Account thisAccount = AccountManager.getAccount();
        System.assert(thisAccount!= null);
        System.assertEquals('ManishiKalra', thisAccount.Name);
    }
   
    // Helper method
    static Id createTestRecord() {
        // Create test record
        Account accounttest = new Account(
        Name = 'ManishiKalra');
        insert accounttest;
        return accounttest.Id;
    }  
    
    static contact createTestContact(id testAccountId){
        Contact contactTest = new Contact();
    contactTest.LastName = 'TestContact';
        contactTest.AccountId = testAccountId;
        insert contactTest;
        return contacttest;
    }
}
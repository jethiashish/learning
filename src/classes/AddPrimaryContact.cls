public class AddPrimaryContact implements queueable{
	private Contact con;
    private String statename;
    
    public AddPrimaryContact(Contact con,String state){
        this.con = con;
        this.statename = state;
    }
    public void execute(QueueableContext context) {
		list<Account> listAccount = [Select id from account where BillingState =:Statename];
        list<Contact> listcon = new list<Contact>();
        if(!listAccount.isEmpty()){
            for(Account eachAccount: listAccount){
                Contact newcon = con.clone(false,false,false,false);
                newcon.AccountId = eachAccount.id;
                listcon.add(newcon);
            }  
        }
        if(!listcon.isEmpty()){
            Insert listcon;
        }
    }
}
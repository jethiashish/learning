/*
This
is 
Learning
BitBucket
Repo and its fun
*/
public class accountActive {
    public static void checkActive(list<Account> listAccounts){
        set<id> setofAccountid = new set<id>();
        map<id,list<Contact>> mapofContacts = new map<id,list<Contact>>();
        
        for(Account eachAccount: listAccounts){
            setofAccountid.add(eachAccount.id);
        }
        
        list<Account> newAccountlist = [Select id,isActive__c from Account where id IN: setofAccountid];
        list<Account> newlistwithContact = [Select id,(Select id,isActive__c from Contacts) from Account where id IN: setofAccountid ];
        for(Account eachAccount: newlistwithContact){
            mapofContacts.put(eachAccount.id, eachAccount.contacts);
        }
        for(Account eachAccount:listAccounts){
            if(eachAccount.isActive__c == False){
                list<Contact> newUpdatedContact = new list<Contact>();
                List<Contact> listContacts = mapofContacts.get(eachAccount.Id);
                for (Contact eachContact : listContacts){
                    eachContact.isActive__c = False;
                    newUpdatedContact.add(eachContact);
                }
                Update newUpdatedContact;
            }
            else if(eachAccount.isActive__c == True){
                list<Contact> newUpdatedContact = new list<Contact>();
                List<Contact> listContacts = mapofContacts.get(eachAccount.Id);
                for (Contact eachContact : listContacts){
                    eachContact.isActive__c = True;
                    newUpdatedContact.add(eachContact);
                }
                Update newUpdatedContact;
            }
        }
    }
}
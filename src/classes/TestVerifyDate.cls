@isTest
private class TestVerifyDate {
    testmethod static void checkwithin30Date(){
        test.startTest();
        Date Date1 = System.today();
        Date Date2 = System.today().addDays(15);
        Date checkdate = VerifyDate.CheckDates(Date1, date2);
        test.stopTest();
        System.assertEquals(System.today().addDays(15), checkdate);
    }
    testmethod static void checkLastDate(){
        Date Date1 = System.today();
        Date Date2 = System.today().addDays(-15);
        Integer totalDays = Date.daysInMonth(date1.year(), date1.month());
		Date lastDay = Date.newInstance(date1.year(), date1.month(), totalDays);
        test.startTest();
        Date checkdate = VerifyDate.CheckDates(Date1, date2);
        test.stopTest();
        System.assertEquals(lastDay, checkdate);
    }
}
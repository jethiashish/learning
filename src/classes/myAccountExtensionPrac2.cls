public class myAccountExtensionPrac2 {

    public string accountNameText{get;set;}
    public string accountInfoText{get;set;}
    public Boolean accountinfo{get;set;}
    public Account acc{get;set;}
    public list<Account> accountList{get;set;}
            
    public myAccountExtensionPrac2(){  
          getAllAccounts();
    }
    
    private void getAllAccounts(){
        accountinfo = true;
        accountList =[Select id,name from Account];
    }
      
    public PageReference getselectedAccount(){
        system.debug('mergefieldtext: '+accountNameText);
        if(String.isBlank(accountNameText)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Enter Some Value to Filter'));
            getAllAccounts();
        }
        else{
            accountList =[Select id,name from Account where name =:accountNameText];
            System.debug('Inside first else'+accountList);
        }  
                      
        if(!accountList.isEmpty()){
            accountinfo = true;
        }
        else{
            System.debug('Inside second else');
            accountinfo = false;
            accountInfoText = 'No records to display';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Record to Display'));
            System.debug('accountinfo--> '+ accountinfo);
         }
        return null;
    
    }

}
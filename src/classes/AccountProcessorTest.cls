@isTest
private class AccountProcessorTest {
    testmethod static void testscenario1(){
        Account TestAccount = AccountProcessorTest.createAccount('TestAccount1');
        Contact TestContact1 = AccountProcessorTest.createcontact('TestContact1',TestAccount.id);
        Contact TestContact2 = AccountProcessorTest.createcontact('TestContact2',TestAccount.id);
        list<id> listaccount = new list<id>();
        listaccount.add(TestAccount.id);
        Test.startTest();
        AccountProcessor.countContacts(listaccount);
        Test.stopTest();
        Account checkacc = [select id,number_of_contacts__c from account where name = 'TestAccount1'order by createddate DESc Limit 1];
        system.assertEquals(2, checkacc.Number_of_Contacts__c);
    }
    testmethod static void testscenario2(){
        Account TestAccount = AccountProcessorTest.createAccount('TestAccount1');
        list<id> listaccount = new list<id>();
        listaccount.add(TestAccount.id);
        Test.startTest();
        AccountProcessor.countContacts(listaccount);
        Test.stopTest();
        Account checkacc = [select id,number_of_contacts__c from account where name = 'TestAccount1'order by createddate DESc Limit 1];
        system.assertEquals(0, checkacc.Number_of_Contacts__c);
    }
    public static Account createAccount(String name){
        Account acc = new Account();
        acc.name = name;
        insert acc;
        return acc;
    }
    public static Contact createcontact(String lastname, ID accid){
        Contact con = new Contact();
        con.AccountId = accid;
        con.lastname = lastname;
        insert con;
        return con;
    }
}
global class batchinsertfeeder implements iterator<SObject>{
    global Account[] source;
    
    global batchinsertfeeder(Account[] source) {
        this.source = source;
    }
    
    global SObject next() {
        return source.remove(0);
    }
    
    global boolean hasNext() {
        return source!=null && !source.isempty();
    }
}
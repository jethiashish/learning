public class ActivityOnAccountController {
	private set<ID> setofaccountid = new set<ID>();
    public id accountid;
    private List<Task> allTask = new list<Task>();
    private list<Event> allEvent = new list<Event>();
    public list<activityWrapper> allTaskwrapper{get;set;}
    public list<activityWrapper> allEventwrapper{get;set;}
    Public ApexPages.StandardSetController setcon{get;set;}
    Public ApexPages.StandardSetController setcon2{get;set;}
    public Boolean ShowTaskDetails{get;set;}
    public Boolean ShowEventDetails{get;set;}
    private Integer ListSize = 3;
    private Integer counter = 0;
    public integer totalsize;
    private Integer ListSize_Event = 3;
    private Integer counter_Event = 0;
    public integer totalsize_Event;
    
    public ActivityOnAccountController(Apexpages.StandardController controller){
        this.accountid = (id)controller.getRecord().id;
        ShowTaskDetails = False;
        ShowEventDetails = False;
        setofaccountid = getChildAccount(accountid);
        totalsize = [select count() from Task where Whatid In:setofaccountid];
        totalsize_Event = [select count() from Event where Whatid In:setofaccountid];
        getOnlyTask();
        getOnlyEvent();
        //getAllActivity();
    }
	
    private set<ID> getChildAccount(ID accID){
        Id accountId = accID;
        Account[] allChildren = new Account[]{};
        Set < Id > parentIds = new Set < Id > {accountId};
        Set < Id > finalAccIds = new Set < Id > {accountId};    
        Account[] children;
        do {
            children = [select Id, Name from Account where ParentId in: parentIds];
            allChildren.addAll(children);
            parentIds.clear();
            for (Account child: children) {
                parentIds.add(child.Id);
                finalAccIds.add(child.Id);
            }
        } while (children.size() > 0);
        system.debug('####all childeren ##' + allChildren);
        system.debug('####parentIds ##' + parentIds);
        system.debug('####finalAccIds ##' + finalAccIds);
        return finalAccIds;
        
    }
    private void getOnlyTask(){
        allTaskwrapper = new list<activitywrapper>();
        allTask = [select ID,Subject,TaskSubType,Status,Whatid,What.name,Priority,Who.name from Task
                   where whatid IN: setofaccountid Limit:ListSize Offset:counter];
        if(!allTask.isEmpty()){
            for(Task eachTask : allTask){
                Event dummyEvent = new Event(subject = 'dummy');
                activityWrapper eachAct = new activityWrapper(False,eachTask,dummyEvent);
                system.debug('Checking 1');
                System.debug('eachAct--->'+eachAct);
                allTaskwrapper.add(eachAct);   
            }
        }
        if(!allTaskwrapper.isEmpty()){
            ShowTaskDetails = True;
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No Records to Display'));
        }
    }
    
    private void getOnlyEvent(){
        allEventwrapper = new list<activitywrapper>();
        allEvent = [select ID,Subject,EventSubtype,Whatid,What.name,Who.name from event where
                    whatid IN: setofaccountid Limit:ListSize_Event Offset:counter_Event];
        if(!allEvent.isEmpty()){
            for(Event eachEvent : allEvent){
                Task DummyTask = new Task(subject = 'dummy');
                activityWrapper eachAct = new activityWrapper(False,DummyTask,eachEvent);
                allEventwrapper.add(eachAct);
            }
        }
        if(!allEventwrapper.isEmpty()){
            ShowEventDetails = True;
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No Records to Display'));
        }
    }
    
    Public Pagereference taskNext(){
        Counter += ListSize;
        getOnlyTask();
        return Null;
    }
    Public Pagereference taskFirst(){
        Counter = 0;
        getOnlyTask();
        return Null;
    }
    Public Pagereference taskprevious(){
        Counter -= ListSize;
        getOnlyTask();
        return Null;
    }
    Public Pagereference taskLast(){
        counter = totalsize - math.mod(totalsize, listsize);
        getOnlyTask();
        return null;
    }
    
    public Boolean getDisablePrevious() { 
        //this will disable the previous and beginning buttons
        if (counter>0) return false; else return true;
    }
    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + listsize < totalsize) return false; else return true;
    }
    
    Public Pagereference EventNext(){
        Counter_Event += ListSize_Event;
        getOnlyEvent();
        return Null;
    }
    Public Pagereference EventFirst(){
        Counter_Event = 0;
        getOnlyEvent();
        return Null;
    }
    Public Pagereference Eventprevious(){
        Counter_Event -= ListSize_Event;
        getOnlyEvent();
        return Null;
    }
    Public Pagereference EventLast(){
        counter_Event = totalsize_Event - math.mod(totalsize_Event, listsize_Event);
        getOnlyEvent();
        return null;
    }
    
    public Boolean getDisablePrevious_Event() { 
        //this will disable the previous and beginning buttons
        if (counter_Event>0) return false; else return true;
    }
    public Boolean getDisableNext_Event() { //this will disable the next and end buttons
        if (counter_Event + listsize_Event < totalsize_Event) return false; else return true;
    }
    /*Private void getAllActivity(){
        allTaskwrapper = new list<activitywrapper>();
        allEventwrapper = new list<activitywrapper>();
        allTask = [select ID,Subject,TaskSubType,Status,Whatid,What.name,Priority,Who.name from Task where whatid IN: setofaccountid];
        
        allEvent = [select ID,Subject,EventSubtype,Whatid,What.name,Who.name from event where whatid IN: setofaccountid];
        
        if(!allTask.isEmpty()){
            //setcon = new Apexpages.StandardSetController(allTask);
            //setcon.setPageSize(4);
            for(Task eachTask : allTask){
                Event dummyEvent = new Event(subject = 'dummy');
                activityWrapper eachAct = new activityWrapper(False,eachTask,dummyEvent);
                system.debug('Checking 1');
                System.debug('eachAct--->'+eachAct);
                allTaskwrapper.add(eachAct);   
            }
        }
        if(!allEvent.isEmpty()){
            //setcon2 = new Apexpages.StandardSetController(allEvent);
            //setcon2.setPageSize(4);
            for(Event eachEvent : allEvent){
                Task DummyTask = new Task(subject = 'dummy');
                activityWrapper eachAct = new activityWrapper(False,DummyTask,eachEvent);
                allEventwrapper.add(eachAct);
            }
        }
        if(!allTaskwrapper.isEmpty()){
            ShowTaskDetails = True;
        }
        if(!allEventwrapper.isEmpty()){
            ShowEventDetails = True;
        }
        if(allTaskwrapper.isEmpty() || allEventwrapper.isEmpty()){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No Records to Display'));
        }
    }*/
       
    public class activityWrapper{
        public Boolean isSelected{get;set;}
        Public ID activityID{get;set;}
        public String Subject{get;set;}
        public String activityType{get;set;}
        public String Status{get;set;}
        public String relatedTo {get;set;}
        public ID relatedToID {get;set;}
        public String forWhom {get;set;}
        public String priority {get;set;}
        
        
        public activityWrapper(Boolean selected,Task activityTask,Event activityEvent){
            this.isSelected = selected;
            if(activityEvent.Subject == 'dummy'){
                System.debug('Inside Task IF');
                activityID = activityTask.ID;
                Subject = activityTask.Subject;
                activityType = activityTask.TaskSubtype;
                Status = activityTask.Status;
                relatedTo = activityTask.What.name;
                relatedToID = activityTask.WhatId;
                forWhom = activityTask.who.name;
                priority = activityTask.Priority;
            }
            else if(activityTask.Subject == 'dummy'){
                System.debug('Inside Event IF');
                activityID = activityEvent.ID;
                Subject = activityEvent.Subject;
                activityType = activityEvent.EventSubtype;
                relatedTo = activityEvent.What.name;
                relatedToID = activityEvent.WhatId;
                forWhom = activityEvent.who.name;
            }
        }
    }
}
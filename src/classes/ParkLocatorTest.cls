@isTest
private class ParkLocatorTest {

    static testMethod void testCallout() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
        // Call the method that invokes a callout
        list<String> result = ParkLocator.country('India');
        // Verify that a fake result is returned
        list<String> countryNames = new list<String>();
        countryNames.add('India');
        countryNames.add('Germany');
        countryNames.add('Japan');
        System.assertEquals(countryNames, result); 
    }
}
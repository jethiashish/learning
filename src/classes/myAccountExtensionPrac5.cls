public with sharing class myAccountExtensionPrac5 {
    public Account acc{get;set;}
    public list<wrapperclass> wrappedRecord{get;set;}
    
        public myAccountExtensionPrac5(ApexPages.StandardsetController controller) {
        this.acc = (Account)controller.getRecord();
        system.debug(this.acc);
        wrappedRecord = getallProjects();
    }
    
    private static list<wrapperclass> getallProjects(){
        list<Project__c> projectData = [select id,Name,Project_Area__c,Project_Cost__c from Project__c];
        list<wrapperclass> allRecord = new list<wrapperclass>();
        
        for(project__c eachProject : projectData){
            allRecord.add(new wrapperclass(eachProject,false));
        }
        system.debug('All record'+allRecord);
        return allRecord;
    }
    
    
    public class wrapperclass{
        public Project__c projectRecord{get;set;}
        public boolean isSelected {get;set;}
        public wrapperclass(Project__c projectRecord, Boolean isSelected){
            this.projectRecord = projectRecord;
                this.isSelected = isSelected;
        }
    } 
}
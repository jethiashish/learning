global class batchAccountupdate implements Database.batchable<SObject>, Database.stateful {
    global string query;
    
    global batchAccountupdate(string query) {
        this.query = query;
    }

    global database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getquerylocator(query);
    }
    
    global void execute(Database.BatchableContext bc, list<Account> scope) {
        for(Account acc : scope){
            acc.industry = 'Finance';
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext bc) {
    
    }
}
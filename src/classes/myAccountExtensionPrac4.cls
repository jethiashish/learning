public with sharing class myAccountExtensionPrac4 {
        private integer totalRecs = 0;
    private integer OffsetSize = 0;
    private integer LimitSize= 5;
    public list<WrapperClass> Wrappedrecord{get;set;}

    public myAccountExtensionPrac4(){
        
        wrappedrecord = getAllAccounts();
        totalRecs = wrappedrecord.size();
    }
    
    /*public list<WrapperClass> displayAccount(){
       wrappedrecord = getAllAccounts();
        return wrappedrecord;
    }*/
    
    public list<WrapperClass> getAllAccounts(){
        list<Account> listAccount = [Select name, phone,Active__c from Account order by name Limit: LimitSize OFFSET:OffsetSize];
        list<WrapperClass> allRecord = new list<WrapperClass>();
        for(Account acc:listAccount){
            allRecord.add(new WrapperClass(acc,False));
        }
        return allRecord;
    }
    
    public PageReference deactivateAccount(){
        List < Account > selectedAccount = new List < Account > ();
        
        for (WrapperClass wrapRec: wrappedRecord){
            /*Check if record is selected*/
            if (wrapRec.isSelected) {
                wrapRec.isSelected = false;
                wrapRec.accountRecord.Active__c = 'No';
                selectedAccount.add(wrapRec.accountRecord);
            }
        }
        update selectedAccount;
        return null;
    }
    
    public PageReference deactivateAllAccount(){
        List < Account > selectedAccount = new List < Account > ();  
        for (WrapperClass wrapRec: wrappedRecord){
            /*Check if record is selected*/
                wrapRec.isSelected = false;
                wrapRec.accountRecord.Active__c = 'No';
                selectedAccount.add(wrapRec.accountRecord);
        }
        update selectedAccount;
        return null;
    }
    
    public PageReference activateAllAccount(){
        List < Account > selectedAccount = new List < Account > ();  
        for (WrapperClass wrapRec: wrappedRecord){
            /*Check if record is selected*/
                wrapRec.isSelected = false;
                wrapRec.accountRecord.Active__c = 'Yes';
                selectedAccount.add(wrapRec.accountRecord);
        }
        update selectedAccount;
        return null;
    }
    
    public PageReference activateAccount(){
        List < Account > selectedAccount = new List < Account > ();
        
        for (WrapperClass wrapRec: wrappedRecord){
            /*Check if record is selected*/
            if (wrapRec.isSelected) {
                wrapRec.isSelected = false;
                wrapRec.accountRecord.Active__c = 'Yes';
                selectedAccount.add(wrapRec.accountRecord);
            }
        }
        update selectedAccount;
        return null;
    }
    
    public PageReference FirstPage()
    {
        OffsetSize = 0;
        wrappedrecord = getAllAccounts();
        return null;
    }
    public PageReference previous()
    {
        OffsetSize -= LimitSize;
        wrappedrecord = getAllAccounts();
        return null;
    }
    public PageReference next()
    {
        OffsetSize += LimitSize;
        wrappedrecord = getAllAccounts();
        return null;
    }
    public PageReference LastPage()
    {
        OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
        wrappedrecord = getAllAccounts();
        return null;
    }
        
    public boolean getprev()
    {
        if(OffsetSize == 0)
            return true;
        else
            return false;
    }
    public boolean getnxt()
    {
        if((OffsetSize + LimitSize) > totalRecs)
            return true;
        else
            return false;
    }    
    public class WrapperClass {
        public Boolean isSelected {get;set;}

        public Account accountRecord {get;set;}
        public WrapperClass(Account accountRecord, Boolean isSelected) {
            this.accountRecord = accountRecord;
            this.isSelected = isSelected;
        }
    }
}
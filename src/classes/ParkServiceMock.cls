@isTest
global class ParkServiceMock  implements WebServiceMock {
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            // start - specify the response you want to send
            ParkService.byCountryResponse response_x = new ParkService.byCountryResponse();
            list<String> countryNames = new list<String>();
            countryNames.add('India');
            countryNames.add('Germany');
            countryNames.add('Japan');
            response_x.return_x = countrynames;
                // end
                response.put('response_x', response_x); 
        }
}
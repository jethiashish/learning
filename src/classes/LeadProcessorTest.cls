@isTest
private class LeadProcessorTest {
    testmethod static void checkLead(){
        LeadProcessorTest.createLead();
        Test.startTest();
        LeadProcessor abc = new LeadProcessor();
        ID Batch = Database.executeBatch(abc);
        Test.stopTest();
        Lead testLead = [Select id,LeadSource from Lead where lastname = 'TestLead1'];
        System.assertEquals('Dreamforce', testLead.LeadSource);
    }
    public static void createLead(){
        List<Lead> listLead = new List<Lead>();
        for(Integer i=1;i<=200;i++){
            Lead eachLead = new lead();
            eachlead.LastName = 'TestLead'+ String.valueOf(i);
            eachLead.Company = 'TestCompany';
            ListLead.add(eachLead);
        }
        if(ListLead.size() > 0){
            insert listLead;
        }
    }
}
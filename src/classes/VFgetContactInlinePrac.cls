public with sharing class VFgetContactInlinePrac {
    public boolean contactavailable{get;set;}
    public boolean showcontact{get;set;}
    public boolean hidebutton{get;set;}
    public list<contact> contactList{get;set;}
    private list<contact> allcontact;
    private id accountid;
    public Account acc{get;set;}
    
    public VFgetContactInlinePrac(ApexPages.StandardController controller){
        accountid = (ID)controller.getRecord().id;
        getallContacts(accountid);
        showcontact = false;
    }
    
    private void getallContacts(ID accountid){
        allcontact = [select id,lastname,email,phone,title from contact where accountid =: accountid];
        if(allcontact.isEmpty()){
            contactavailable = False;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Records to Display'));
        }
        else{
            contactavailable = True;
            hidebutton = True;
        }
    }
    
    public Pagereference getcontacts(){
        if(contactavailable == True){
            contactList = allcontact;
            showcontact = true;
            hidebutton = false;
        }
        return null;
    }
}
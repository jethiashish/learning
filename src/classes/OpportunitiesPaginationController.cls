public class OpportunitiesPaginationController{
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    public ApexPages.StandardSetController setCon{get;set;}
         
    public OpportunitiesPaginationController(){
        size=5;
        getOppList();
    }
     
  /*  public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [select id,Name,AccountId,Account.name,Amount,StageName,CloseDate,LastModifiedDate from Opportunity]));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }            
            return setCon;
        }
        set;
    }
 */
     private void getOppList(){
         list<Opportunity> opList = [select id,Name,AccountId,Account.name,Amount,StageName,CloseDate,LastModifiedDate from Opportunity];
        //Set standard set controller 
        setCon = new ApexPages.StandardSetController(opList);
        setCon.setPageSize(size);        
     }
 
    // Initialize setCon and return a list of record    
     
    public List<Opportunity> getOpportunities() {
         return (List<Opportunity>) setCon.getRecords();
    }
}
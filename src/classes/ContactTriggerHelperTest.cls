@isTest
Public class ContactTriggerHelperTest {
    //Setup data for Testing
	@testSetup
    public static void createData(){
        User userid = [Select id from user where id =: userinfo.getUserId()];
        System.runAs(userid){
            Account TestAccount = ContactTriggerHelper_TestDataFactory.createAccount('TestAccount', True);
            Contact TectContact1 = ContactTriggerHelper_TestDataFactory.createContact('Test','Contact1', TestAccount.id);
            Contact TectContact2 = ContactTriggerHelper_TestDataFactory.createContact('Test','Contact2', TestAccount.id);
            Contact TectContact3 = ContactTriggerHelper_TestDataFactory.createContact('Test','Contact3', TestAccount.id);
        }
    }
    //Check Insertion of Contact
    testmethod Static void testInsertContact(){
        Account TestAccount = [Select id from Account where name= 'TestAccount' order by createdDate DESC Limit 1];
        Test.startTest();
        Contact TectContact4 = ContactTriggerHelper_TestDataFactory.createContact('Test','Contact4', TestAccount.id);
        Test.stopTest();
        Account newacc = [Select Contacts_Last_Name__c from Account where name = 'TestAccount' order by createdDate limit 1];
        System.debug('Contact Last Name on Account-->'+newacc.Contacts_Last_Name__c);
        System.assertEquals('Contact1,Contact2,Contact3,Contact4', newacc.Contacts_Last_Name__c);
    }
    //Check Updation of Contact
    testmethod Static void testUpdateContact(){
        Account TestAccount = [Select id from Account where name= 'TestAccount' order by createdDate DESC Limit 1];
        Test.startTest();
        
        Contact TestContact4 = ContactTriggerHelper_TestDataFactory.createContact('Test','Contact4', TestAccount.id);
        Account newacc1 = [Select Contacts_Last_Name__c from Account where name = 'TestAccount' order by createdDate limit 1];
        System.assertEquals('Contact1,Contact2,Contact3,Contact4', newacc1.Contacts_Last_Name__c);
        TestContact4.LastName = 'NewLastname';
        update TestContact4;
        Test.stopTest();
        
        Account newacc2 = [Select Contacts_Last_Name__c from Account where name = 'TestAccount' order by createdDate limit 1];
        System.debug('Contact Last Name on Account-->'+newacc2.Contacts_Last_Name__c);
        System.assertEquals('Contact1,Contact2,Contact3,NewLastname', newacc2.Contacts_Last_Name__c);
    }
    //Check Deletion of Contact
    testmethod Static void testDeleteContact(){
        Account TestAccount = [Select id from Account where name= 'TestAccount' order by createdDate DESC Limit 1];
        Test.startTest();
        
        Contact TestContact4 = ContactTriggerHelper_TestDataFactory.createContact('Test','Contact4', TestAccount.id);
        Account newacc1 = [Select Contacts_Last_Name__c from Account where name = 'TestAccount'];
        System.assertEquals('Contact1,Contact2,Contact3,Contact4', newacc1.Contacts_Last_Name__c);
        Delete TestContact4;
        Test.stopTest();
        
        Account newacc = [Select Contacts_Last_Name__c from Account where name = 'TestAccount' order by createdDate limit 1];
        System.debug('Contact Last Name on Account-->'+newacc.Contacts_Last_Name__c);
        System.assertEquals('Contact1,Contact2,Contact3', newacc.Contacts_Last_Name__c);
    }
     //test bulk Insertion
    testmethod Static void testInsertContact_2(){
        Account TestAccount2 = ContactTriggerHelper_TestDataFactory.createAccount('TestAccount2', True);
        
        Test.startTest();
        ContactTriggerHelper_TestDataFactory.createBulkContact(TestAccount2.id); //create Bulk Contact
        Test.stopTest();
        
        Account newacc2 = [Select Contacts_Last_Name__c from Account where name = 'TestAccount2' order by createdDate limit 1];
        System.debug('Contact Last Name on Account-->'+newacc2.Contacts_Last_Name__c);
        System.assert(String.isnotBlank(newacc2.Contacts_Last_Name__c));
    }
    //Negative Testing
    /*testmethod Static void testNegativeInsertContact(){
        Account TestAccount2 = ContactTriggerHelper_TestDataFactory.createAccount('TestAccount2', True);
        
        Test.startTest();
        ContactTriggerHelper_TestDataFactory.createBulkContact(TestAccount2.id); //create Bulk Contact
        Test.stopTest();
        
        Account newacc2 = [Select Contacts_Last_Name__c from Account where name = 'TestAccount2' order by createdDate limit 1];
        System.debug('Contact Last Name on Account-->'+newacc2.Contacts_Last_Name__c);
        System.assert(String.isnotBlank(newacc2.Contacts_Last_Name__c));
    }*/
}
public class ContactTriggerHelper {
    public static void populateLastName(list<Contact> listContact){
        try{
        set<id> setofAccountid = new set<id>();
        map<ID,Account> mapofAccount = new Map<ID,Account>();
        map<id,String> mapofContactname = new Map<ID,String>();
        set<Account> setofAllAccounts = new set<Account>();
        
        
        for(Contact eachContact : listContact){
            if(String.isNotBlank(eachContact.Accountid)){
                setofAccountid.add(eachContact.Accountid);
            }
        }
        
        list<Account> allAccount = [Select id, Contacts_last_name__c from Account where id in: setofAccountid];
        
        list<Contact> allContact = [Select id,accountid, Lastname from Contact where Accountid in: setofAccountid order by createddate ASC];
        
        for(Contact eachContact: allContact){
            if(mapofContactname.isEmpty()){
                mapofContactname.put(eachContact.accountid,eachContact.LastName);
            }
            else{
                if(mapofContactname.containsKey(eachContact.AccountId)){
                    String lastName = mapofContactname.get(eachContact.AccountId);
                    lastName += ','+ eachContact.LastName;
                    mapofContactname.put(eachContact.AccountId,lastName);
                }
                else{
                    String lastName;
                    lastName = eachContact.LastName;
                    mapofContactname.put(eachContact.AccountId,lastName);
                }
            } 
        }
        
        for(Account eachAccount : allAccount){
            mapofAccount.put(eachAccount.id, eachAccount);
        }
        
        
        ContactTriggerHelper.clearAllData(allAccount); // Method Call to Delete Old Data.
        
        
       	
        for(Contact eachContact : listContact){
            String allDataString = mapofContactname.get(eachContact.AccountId);
            Account oneAccount = mapofAccount.get(eachContact.AccountId);
            oneAccount.Contacts_Last_Name__c = allDataString;
            setofAllAccounts.add(oneAccount);
        }
        
        if(!setofAllAccounts.isEmpty()){
            system.debug('Set of accounts--->'+setofAllAccounts);
            system.debug('Set of accounts Size--->'+setofAllAccounts.size());
            list<Account> allAccountList = new List<Account>();
            allAccountList.addAll(setofAllAccounts);
            Database.update(allAccountList);
        }
        }
        catch(Exception e){
            System.debug('Excpetion Caught:'+e.getMessage());
        }
    }
    
    private static void clearAllData(List<Account> ListAccount){
        list<Account> allAccount = new List<Account>();
        for(Account eachAccount : ListAccount){
            eachAccount.Contacts_Last_Name__c = Null;
            allAccount.add(eachAccount);
        }
        if(!allAccount.isEmpty()){
            Database.update(allAccount);
        }
    }
}